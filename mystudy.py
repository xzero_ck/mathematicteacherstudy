import itertools
mycount = 0
n= 50
# k = 1
for k in range(n):
    lb = k/n
    ub = (k+1)/n
    for m in itertools.count(1):
        if ub <= 1/m :
            continue
        elif 1/m >= lb :
            mycount +=1
            # print(f'm={m},k={k}')
            break
        else :
            break

print(mycount)

def myf(n):
    mycount = 0
    for k in range(n):
        lb = k / n
        ub = (k + 1) / n
        for m in itertools.count(1):
            if ub <= 1 / m:
                continue
            elif 1 / m >= lb:
                mycount += 1
                # print(f'm={m},k={k}')
                break
            else:
                break

    return mycount
print(myf(20))

for i in range(100,3000):
    print(i,myf(i)/i**0.5)
